const packager = require('electron-packager');
const electronInstaller = require('electron-winstaller');

async function build(options) {
    const appPaths = await packager(options);

    console.log(`✅ App build ready in: ${appPaths.join('\n')}, creating installer...`);

    try {
        await electronInstaller.createWindowsInstaller({
            appDirectory: './dist/MusicQuiz-win32-x64',
            outputDirectory: './dist/installer',
            authors: 'ysjeon',
            description: 'Svelte app made with Electron',
            exe: 'MusicQuiz.exe'
        });

        console.log('💻 Installer is created in dist/installer');
    } catch (e) {
        console.log(`The following error occured: ${e.message}`);
    }
};

build({
    name: 'MusicQuiz',
    dir: './',
    out: 'dist',
    overwrite: true,
    asar: true,
    platform: 'win32',
});